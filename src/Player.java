
public class Player {
	private char Name  ;
	private int Win ; 
	private int Draw ;
	private int Lose ;
	
	Player(char name){
		this.Name = name ;
		Win = 0 ;
		Draw = 0 ;
		Lose = 0;
	}
	
	public char getName(){
		return Name ;
	}
	
	public int getWin() {
		return Win ;
	}
	
	public int getDraw() {
		return Draw;
	}
	public int getLose() {
		return Lose;
	}
	public void addWin() {
		Win++;
	}
	
	public void addLose() {
		Lose++;
	}
	
	public void addDraw() {
		Draw++;
	}

}
