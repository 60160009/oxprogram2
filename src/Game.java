import java.util.*;
public class Game {
	private Board board;
	private Player X;
	private Player O;
	
	public Game() {
		X = new Player('X');
		O = new Player('O');
		board = new Board(X,O);
	}
	
	public void play() {
		showWelcome();
		for(;;) {
			showTable();
			input();
			if (board.isFinish() == true) {
				break;
			}
			board.switchTurn();
		}
		showTable();
		showResult();
		showStat();
		showBye();
		board.clearBoard();
	}
	
	public void showWelcome() {
		System.out.println(board.getWelcome()); ;
	}
	public void showTable() {
		System.out.println(" " + " 1 " + "2" + " 3 ");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < 3; j++) {
				System.out.print("|" + board.getTable()[i][j] );
			}
			System.out.print("|");
			System.out.println();
		}
	}
	public void showTurn() {
		System.out.println(board.getCurrent());
	}
	
	public void input() {
		Scanner kb = new Scanner(System.in);
		for(;;) {
			System.out.println("Turn" + " " +  board.getCurrent().getName());
			System.out.print("Plz Choose position(R,C) : ");
			String row = kb.next();
			String col = kb.next();
			if (board.setTable(row,col) == true) {
				break ;
			}
			showTable();
		}
	}
	
	public void showResult() {
		if(board.getWinner() != null) {
			System.out.println( board.getWinner().getName()+ ": Win" );
		}else {
			System.out.println( "DRAW" );
		}
	}
	public void showBye() {
		System.out.println("~~~~~~~~~~~ Bye ~~~~~~~~~~~");
	}
	
    public void showStat() {
    	System.out.println(" Player X Win: "+X.getWin()+" Lose: "+X.getLose()+" Draw: "+X.getDraw());
    	System.out.println(" Player O Win: "+O.getWin()+" Lose: "+O.getLose()+" Draw: "+O.getDraw());
    }
    
}
